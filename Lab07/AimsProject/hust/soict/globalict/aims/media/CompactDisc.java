package soict.globalict.aims.media;

import java.util.*;

import static soict.globalict.aims.media.Track.availTrack;

public class CompactDisc extends Disc implements Playable {

    static public final ArrayList<Media> availCDs = new ArrayList<>(Arrays.asList(
            new CompactDisc("Compact Disc 1", "Cate 1", 5.0f, "Artist 1",
                    new ArrayList<>(Arrays.asList(availTrack.get(0), availTrack.get(1)))),
            new CompactDisc("Compact Disc 2", "Cate 2", 10.0f, "Artist 2",
                    new ArrayList<>(Collections.singletonList(availTrack.get(2)))),
            new CompactDisc("Compact Disc 3", "Cate 3", 15.0f, "Artist 3",
                    new ArrayList<>(Arrays.asList(availTrack.get(2), availTrack.get(0))))
    ));

    private String artist;
    private ArrayList<Track> tracksList = new ArrayList<>();

    public CompactDisc(String title) {
        super(title);
    }

    public CompactDisc(String title, String category) {
        super(title, category);
    }

    public CompactDisc(String title, String category, float cost) {
        super(title, category, cost);
    }

    public CompactDisc(String title, String category, float cost, String artist, ArrayList<Track> tracksList) {
        super(title, category, cost);
        this.artist = artist;
        if (checkDupTrack(tracksList))
            this.tracksList = tracksList;
        else this.tracksList = null;
        length = getLength();
    }

    public boolean checkDupTrack(ArrayList<Track> tracksList) {
        return (new HashSet<>(tracksList)).size() == tracksList.size();
    }

    public boolean addTrack(Track aTrack) {
        if (tracksList.contains(aTrack)) {
            System.out.println("Duplicate track");
            return false;
        }

        try {
            tracksList.add(aTrack);
            System.out.printf("Added\n%s\n\n", aTrack);
            length += aTrack.getLength();
            return true;
        } catch (Exception ex) {
            System.out.printf("Cannot add\n%s\nUnexpected error", aTrack);
            return false;
        }
    }

    public boolean removeTrack(Track aTrack) {
        if (tracksList.remove(aTrack)) {
            System.out.printf("Removed\n%s\n\n", aTrack);
            length -= aTrack.getLength();
            return true;
        } else {
            System.out.printf("Cannot add\n%s\nUnexpected error", aTrack);
            return false;
        }
    }

    @Override
    public int getLength() {
        int totalLength = 0;
        for (Track aTrack : tracksList) {
            totalLength += aTrack.getLength();
        }
        return totalLength;
    }

    @Override
    public void play() {
        for (Track aTrack : tracksList) {
            aTrack.play();
        }
    }

    @Override
    public String toString() {
        return String.format("CDs - %s - %s - %s - %d: $%.2f", title, category, artist, length, cost);
    }
}
