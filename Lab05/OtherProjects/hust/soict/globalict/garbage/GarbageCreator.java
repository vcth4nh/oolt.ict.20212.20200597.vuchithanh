package soict.globalict.garbage;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class GarbageCreator {
    public static void main(String[] args) {
        long charRead = 0;
        String fileName = "garbage.txt";
        try {
            FileReader myFile = new FileReader(fileName);
            Scanner myReader = new Scanner(myFile);
            String s = "";
            while (myReader.hasNext()) {
                s += myReader.next();
            }
            myReader.close();
            charRead = s.length();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        System.out.printf("Read %d chars from file %s", charRead, fileName);
    }
}
