package soict.globalict.aims.media;

public abstract class Media implements Comparable<Media> {
    protected static long count = 0;
    protected long id;
    protected String title;
    protected String category;
    protected float cost;

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }


    public float getCost() {
        return cost;
    }


    public Media(String title) {
        this.title = title;
        id = count++;
    }

    public Media(String title, String category) {
        this.title = title;
        this.category = category;
        id = count++;
    }

    public Media(String title, String category, float cost) {
        this.title = title;
        this.category = category;
        this.cost = cost;
        id = count++;
    }

    @Override
    public boolean equals(Object o) {
        if (getClass() != o.getClass())
            return false;

        return id == ((Media) o).id;
    }

    @Override
    public int compareTo(Media o) {
        if (getClass() != o.getClass())
            return 0;
        return title.compareTo(o.title);
    }
}
