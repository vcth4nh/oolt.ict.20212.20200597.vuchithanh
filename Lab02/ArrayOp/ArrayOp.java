import java.util.Arrays;

public class ArrayOp {
    public static void main(String[] args) {
        int[] arr = {3, 5, 7, -2, 2, 200, 5};
        System.out.printf("Original array: %s\n", Arrays.toString(arr));

        Arrays.sort(arr);
        System.out.printf("Sorted array %s\n", Arrays.toString(arr));

        System.out.printf("Average %f\n", Arrays.stream(arr).average().orElse(Double.NaN));
    }
}
