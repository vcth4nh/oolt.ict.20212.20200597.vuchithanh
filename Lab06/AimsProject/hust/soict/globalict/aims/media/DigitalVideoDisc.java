package soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class DigitalVideoDisc extends Media {
    private String director;
    private int length;

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public DigitalVideoDisc(String title) {
        super(title);
    }

    public DigitalVideoDisc(String title, String category) {
        super(title, category);
    }

    public DigitalVideoDisc(String title, String category, String director) {
        super(title, category);
        this.director = director;
    }

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        super(title, category);
        this.director = director;
        this.length = length;
        this.setCost(cost);
    }


    @Override
    public String toString() {
        return String.format("DVD - %s - %s - %s - %d : $%.2f", getTitle(), getCategory(), director, length, getCost());
    }

    public boolean search(String title) {
        String searchTitle = title.toLowerCase();
        String curTitle = this.getTitle().toLowerCase();
        ArrayList<String> searchTitleTokens = new ArrayList<>(Arrays.asList(searchTitle.trim().split("\\s+")));
        ArrayList<String> curTitleTokens = new ArrayList<>(Arrays.asList(curTitle.trim().split("\\s+")));
        return curTitleTokens.containsAll(searchTitleTokens);
    }
}
