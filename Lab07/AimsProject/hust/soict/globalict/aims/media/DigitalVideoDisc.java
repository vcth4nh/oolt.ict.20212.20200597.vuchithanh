package soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Arrays;

public class DigitalVideoDisc extends Disc implements Playable {
    static public final ArrayList<Media> availDiscs = new ArrayList<>(Arrays.asList(
            new DigitalVideoDisc("Aladdin", "Animation", "Rogers Allers", 90, 18.99f),
            new DigitalVideoDisc("The Lion King", "Animation", "John Musker", 87, 19.95f),
            new DigitalVideoDisc("Star wars", "Science Fiction", "George Lucas", 124, 24.95f)
    ));

    public void setCost(float cost) {
        this.cost = cost;
    }

    public DigitalVideoDisc(String title) {
        super(title);
    }

    public DigitalVideoDisc(String title, String category) {
        super(title, category);
    }

    public DigitalVideoDisc(String title, String category, String director) {
        super(title, category);
        this.director = director;
    }

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        super(title, category);
        this.director = director;
        this.length = length;
        this.cost = cost;
    }


    @Override
    public String toString() {
        return String.format("DVD - %s - %s - %s - %d : $%.2f", getTitle(), getCategory(), director, length, getCost());
    }

    public boolean search(String title) {
        String searchTitle = title.toLowerCase();
        String curTitle = this.getTitle().toLowerCase();
        ArrayList<String> searchTitleTokens = new ArrayList<>(Arrays.asList(searchTitle.trim().split("\\s+")));
        ArrayList<String> curTitleTokens = new ArrayList<>(Arrays.asList(curTitle.trim().split("\\s+")));
        return curTitleTokens.containsAll(searchTitleTokens);
    }

    @Override
    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
        System.out.println();
    }
}
