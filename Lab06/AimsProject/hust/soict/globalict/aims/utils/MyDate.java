package soict.globalict.aims.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class MyDate {
    private int day, month, year;

    public MyDate() {
        LocalDateTime now = LocalDateTime.now();
        day = now.getDayOfMonth();
        month = now.getMonthValue();
        year = now.getYear();
    }

    public MyDate(int day, int month, int year) {
        // TODO: validate input
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(String dateStr) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d yyyy");
        LocalDate date = LocalDate.parse(dateStr, formatter);
        day = date.getDayOfMonth();
        month = date.getMonthValue();
        year = date.getYear();
    }

    public MyDate(String day, String month, String year) {
        String dateStr = String.format("%s %s %s", dateToNumber(day), month, yearToNumber(year));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMMM yyyy");
        LocalDate date = LocalDate.parse(dateStr, formatter);
        this.day = date.getDayOfMonth();
        this.month = date.getMonthValue();
        this.year = date.getYear();
    }


    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public static String yearToNumber(String year) {
        String[] yearToken = year.split(" ");
        String finalYear;
        int secondNumIdx = WordsToNumbersUtil.allowedStrings.indexOf(yearToken[1]);
        if (0 < secondNumIdx && secondNumIdx <= 10) {
            finalYear = WordsToNumbersUtil.convertTextualNumbersInDocument(yearToken[0]);
            finalYear += '0';
            finalYear += WordsToNumbersUtil.convertTextualNumbersInDocument(yearToken[1]);
        } else if (10 < secondNumIdx && secondNumIdx < 29) {
            finalYear = WordsToNumbersUtil.convertTextualNumbersInDocument(yearToken[0]);
            finalYear += WordsToNumbersUtil.convertTextualNumbersInDocument(yearToken[1]);
        } else
            finalYear = WordsToNumbersUtil.convertTextualNumbersInDocument(year);

        return finalYear;
    }

    public static String dateToNumber(String day) {
        final List<String> days = Arrays.asList("zeroth", "first", "second", "third", "fourth", "fifth", "sixth",
                "seventh", "eighth", "ninth", "tenth", "eleventh", "twelfth", "thirteenth", "fourteenth", "fifteenth",
                "sixteenth", "seventeenth", "eighteenth", "nineteenth", "twentieth", "twenty-first", "twenty-second",
                "twenty-third", "twenty-fourth", "twenty-fifth", "twenty-sixth", "twenty-seventh", "twenty-eighth",
                "twenty-ninth", "thirtieth", "thirty-first");
        return Integer.toString(days.indexOf(day.toLowerCase()));
    }

    static public MyDate accept() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input date (e.g. December 13 2003): ");
        return new MyDate(scanner.nextLine());
    }

    public void print() {
        System.out.printf("%s\n", this);
    }

    public void print(String format) {
        System.out.printf("%s\n", this.toString(format));
    }

    @Override
    public String toString() {
        final String[] suffixes =
                //    0     1     2     3     4     5     6     7     8     9
                {"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
                        //    10    11    12    13    14    15    16    17    18    19
                        "th", "th", "th", "th", "th", "th", "th", "th", "th", "th",
                        //    20    21    22    23    24    25    26    27    28    29
                        "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
                        //    30    31
                        "th", "st"};
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("d M yyyy");
        String dateStr = String.format("%d %d %d", day, month, year);
        LocalDate date = LocalDate.parse(dateStr, formatter1);
        String timeFormatStr = String.format("MMMM d'%s' yyyy", suffixes[day]);
        return date.format(DateTimeFormatter.ofPattern(timeFormatStr));
    }

    public String toString(String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d M yyyy");
        String dateStr = String.format("%d %d %d", day, month, year);
        LocalDate date = LocalDate.parse(dateStr, formatter);
        return date.format(DateTimeFormatter.ofPattern(format));
    }

    public static void main(String[] args) {
        MyDate timeObj1 = new MyDate();
        System.out.printf("No argument: %d-%d-%d\n", timeObj1.day, timeObj1.month, timeObj1.year);

        MyDate timeObj2 = new MyDate(4, 4, 2002);
        System.out.printf("3 arguments: %d-%d-%d\n", timeObj2.day, timeObj2.month, timeObj2.year);

        MyDate timeObj3 = accept();
        System.out.printf("1 argument: %d-%d-%d\n", timeObj3.day, timeObj3.month, timeObj3.year);

    }
}
