package soict.globalict.aims.order;

import soict.globalict.aims.media.Media;
import soict.globalict.aims.utils.MyDate;

import java.util.ArrayList;

public class Order {
    public static final int MAX_NUMBER_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERS = 5;

    private final ArrayList<Media> itemsOrdered = new ArrayList<>();

    private static int nbOrders = 0;

    private MyDate dateOrdered;

    public Order() {
        if (nbOrders < MAX_LIMITED_ORDERS) {
            nbOrders++;
            dateOrdered = new MyDate();
        } else {
            System.out.printf("Max orders reached (currently %d orders)\n", nbOrders);
            throw new IllegalArgumentException();
        }
    }

    public MyDate getDateOrdered() {
        return dateOrdered;
    }

    public void setDateOrdered(MyDate dateOrdered) {
        this.dateOrdered = dateOrdered;
    }

    public ArrayList<Media> getItemsOrdered() {
        return itemsOrdered;
    }

    public Media getItemsOrdered(int no) {
        return itemsOrdered.get(no);
    }

    public int size() {
        return itemsOrdered.size();
    }

    public boolean addMedia(Media item) {
        if (itemsOrdered.size() < MAX_NUMBER_ORDERED)
            return itemsOrdered.add(item);

        return false;
    }

    public boolean removeMedia(Media item) {
        return itemsOrdered.remove(item);
    }

    public Media removeMedia(int itemNo) {
        try {
            return itemsOrdered.remove(itemNo);
        } catch (IndexOutOfBoundsException ex) {
            return null;
        }
    }

    public float totalCost() {
        float cost = 0;
        for (Media media : itemsOrdered) {
            cost += media.getCost();
        }
        return cost;
    }

    public void invoiceHead() {
        System.out.println("************************Order************************");
        System.out.printf("Date: %s\n", dateOrdered);
    }

    public void invoiceBody() {
        if (itemsOrdered.size() == 0) {
            System.out.println("Empty order");
        } else
            for (int i = 0; i < itemsOrdered.size(); i++) {
                System.out.printf("%2d. %s\n", i + 1, itemsOrdered.get(i));
            }
        System.out.printf("Total cost: %.2f\n", totalCost());
    }

    public void invoiceFoot() {
        System.out.println("*****************************************************");
    }

    public void invoice() {
        invoiceHead();
        invoiceBody();
        invoiceFoot();
    }

    public void invoice(int randDiscNo) {
        invoiceHead();
        if (randDiscNo != -1) {
            System.out.printf("Disc no. %2d (%s) is the lucky item\n", randDiscNo + 1, itemsOrdered.get(randDiscNo).getTitle());
        }
        invoiceBody();
        invoiceFoot();
    }

//    public int getALuckyItem() {
//        if (itemsOrdered.size() == 0) return -1;
//        int randDiscNo = (int) (Math.random() * itemsOrdered.size());
//        itemsOrdered.get(randDiscNo).setCost(0);
//        return randDiscNo;
//    }

//    @Override
//    public String toString() {
//        return String.format("Order - %s - ",order);
//    }
}
