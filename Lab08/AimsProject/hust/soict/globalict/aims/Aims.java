package soict.globalict.aims;

import soict.globalict.aims.media.*;
import soict.globalict.aims.order.Order;
import soict.globalict.aims.utils.MemoryDaemon;

import java.util.*;

import static soict.globalict.aims.media.Book.availBooks;
import static soict.globalict.aims.media.CompactDisc.availCDs;
import static soict.globalict.aims.media.DigitalVideoDisc.availDiscs;
import static soict.globalict.aims.media.Track.availTrack;

public class Aims {
    static private final ArrayList<Media> availItems = new ArrayList<>();

    static private final Scanner kb = new Scanner(System.in);
    static private final ArrayList<Order> orderList = new ArrayList<>();


    public static void main(String[] args) {
        Thread thread = new Thread(new MemoryDaemon());
        thread.setDaemon(true);
        thread.start();

        availItems.addAll(availDiscs);
        availItems.addAll(availBooks);
        availItems.addAll(availCDs);

        int cmd;

        while (true) {
            showMenu();
            cmd = kb.nextInt();
            kb.nextLine();
            System.out.println("\n");
            switch (cmd) {
                case 1: {
                    orderList.add(new Order());
                    System.out.println("New order created\n");
                    break;
                }
                case 2: {
                    if (isEmptyOrdList()) break;

                    System.out.println("Select Order number to add item");
                    Order order = getOrdProc();
                    if (order == null) break;

                    showAvailItems();

                    int itemNo = kb.nextInt();
                    kb.nextLine();

                    if (itemNo == 0) order.addMedia(processCustomItem());
                    else if (itemNo <= availItems.size()) order.addMedia(availItems.get(itemNo - 1));
                    else {
                        System.out.println("Wrong item number\n");
                        break;
                    }

                    System.out.println("New item added\n");

                    break;
                }
                case 3: {
                    if (isEmptyOrdList()) break;

                    System.out.println("Select order to delete item");
                    Order order = getOrdProc();
                    if (order == null) break;

                    order.invoiceBody();
                    order.invoiceFoot();

                    System.out.print("Select item to delete: ");
                    int itemNo = kb.nextInt() - 1;
                    kb.nextLine();

                    if (itemNo < 0 || itemNo >= order.size()) {
                        System.out.println("Invalid item id\n");
                        break;
                    }

                    Media removedItem = order.removeMedia(itemNo);
                    if (removedItem != null) {
                        System.out.printf("Deleted item:\n%s\n\n", removedItem);
                    } else System.out.println("Failed to remove item\n");

                    break;
                }
                case 4: {
                    if (isEmptyOrdList()) break;

                    System.out.print("Select order to display item (0 to display all orders): ");
                    int orderNo = kb.nextInt() - 1;
                    kb.nextLine();

                    if (orderNo == -1) showAllOrder();
                    else {
                        Order order = getOrdProc(orderNo);
                        if (order == null) break;
                        order.invoice();
                    }
                    break;
                }
                case 0:
                    System.out.println("Thanks for purchasing");
                    return;
                default:
                    System.out.println("Wrong command\n");
            }
        }
    }

    public static void showMenu() {
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.print("Please choose a number: 0-1-2-3-4: ");
    }

    public static void showAvailItems() {
        System.out.println("Available discs: ");
        for (int i = 0; i < availDiscs.size(); i++) {
            System.out.printf("%2d. %s\n", i + 1, availDiscs.get(i));
        }
        System.out.println("------------------------------------------");
        System.out.println("Available books: ");
        for (int i = 0; i < availBooks.size(); i++) {
            System.out.printf("%2d. %s\n", availDiscs.size() + i + 1, availBooks.get(i));
        }
        System.out.println("------------------------------------------");
        System.out.println("Available CDs: ");
        for (int i = 0; i < availCDs.size(); i++) {
            System.out.printf("%2d. %s\n", availDiscs.size() + availBooks.size() + i + 1, availCDs.get(i));
        }
        System.out.println("------------------------------------------");
        System.out.println("Press 0 if you want to order a custom item");
        System.out.print("Your item is: ");
    }

    public static void promptToPlay(Playable item) {
        System.out.print("Do you want to play this item? [y/other]: ");
        String cmd = kb.nextLine();

        if (cmd.equals("y"))
            item.play();
    }

    public static Media processCustomItem() {
        int type = 0;
        while (type != 1 && type != 2 && type != 3) {
            System.out.print("Book (1) or Disc (2) or CDs (3): ");
            type = kb.nextInt();
            kb.nextLine();
        }

        System.out.print("Title: ");
        String title = kb.nextLine();
        System.out.print("Category: ");
        String category = kb.nextLine();
        System.out.print("Cost: ");
        float cost = kb.nextFloat();
        kb.nextLine();

        switch (type) {
            case 1: {
                Book item = new Book(title, category, cost);

                System.out.print("Number of author: ");
                int authorNo = kb.nextInt();
                kb.nextLine();

                for (int i = 1; i <= authorNo; i++) {
                    System.out.printf("Author %d: ", i);
                    item.addAuthor(kb.nextLine());
                }

                return item;
            }
            case 2: {
                System.out.print("Length: ");
                int length = kb.nextInt();
                kb.nextLine();

                System.out.print("Director: ");
                String director = kb.nextLine();

                DigitalVideoDisc item = new DigitalVideoDisc(title, category, director, length, cost);
                promptToPlay(item);

                return item;
            }
            case 3: {
                System.out.print("Artist: ");
                String artist = kb.nextLine();

                System.out.println("Available Track: ");
                for (int i = 0; i < availTrack.size(); i++) {
                    System.out.printf("%2d. %s\n", i + 1, availTrack.get(i));
                }
                System.out.println("------------------------------------------");

                ArrayList<Track> trackList = new ArrayList<>();
                while (true) {
                    System.out.print("Choose track to add (0 to finish): ");
                    int trackNo;
                    trackNo = kb.nextInt() - 1;
                    kb.nextLine();

                    if (trackNo > -1 && trackNo < availTrack.size())
                        trackList.add(availTrack.get(trackNo));
                    else if (trackNo == -1) {
                        if (trackList.size() == 0)
                            System.out.println("Please choose at least one track");
                        else break;
                    } else
                        System.out.println("Invalid track number");
                }

                CompactDisc item = new CompactDisc(title, category, cost, artist, trackList);
                promptToPlay(item);

                return item;
            }
            default:
                System.out.println("Unexpected error");
                return null;
        }
    }

    public static Order getOrdProc() {
        return getOrd(-1);
    }

    public static Order getOrdProc(int orderNo) {
        return getOrd(orderNo);
    }

    public static Order getOrd(int orderNo) {
        if (orderNo == -1) {
            orderNo = kb.nextInt() - 1;
            kb.nextLine();
        }

        if (orderNo < 0 || orderNo >= orderList.size()) {
            System.out.println("Wrong order number\n");
            return null;
        }

        return orderList.get(orderNo);
    }


    public static boolean isEmptyOrdList() {
        if (orderList.size() == 0) {
            System.out.println("No order created\n");
            return true;
        }
        return false;
    }

    private static void showAllOrder() {
        for (int idx = 0; idx < orderList.size(); idx++) {
            System.out.printf("********* Order no %d *********\n", idx);
            orderList.get(idx).invoiceBody();
            System.out.println();
        }
        System.out.println("\n");
    }
}
