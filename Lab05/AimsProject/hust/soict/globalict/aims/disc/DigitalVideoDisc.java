package soict.globalict.aims.disc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class DigitalVideoDisc {
    private String title;
    private String category;
    private String director;
    private int length;
    private float cost;

    public DigitalVideoDisc(String title) {
        this.title = title;
    }

    public DigitalVideoDisc(String title, String category) {
        this.title = title;
        this.category = category;
    }

    public DigitalVideoDisc(String title, String category, String director) {
        this.title = title;
        this.category = category;
        this.director = director;
    }

    public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
        this.title = title;
        this.category = category;
        this.director = director;
        this.length = length;
        this.cost = cost;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return String.format("DVD - %s - %s - %s - %d : $%.2f", title, category, director, length, cost);
    }

    public boolean search(String title) {
        String searchTitle = title.toLowerCase();
        String curTitle = this.title.toLowerCase();
        ArrayList<String> searchTitleTokens = new ArrayList<>(Arrays.asList(searchTitle.trim().split("\\s+")));
        ArrayList<String> curTitleTokens = new ArrayList<>(Arrays.asList(curTitle.trim().split("\\s+")));
        return curTitleTokens.containsAll(searchTitleTokens);
    }
}
