package soict.globalict.aims.media;

import java.util.*;

public class Book extends Media {
    static public final ArrayList<Media> availBooks = new ArrayList<>(Arrays.asList(
            new Book("The Lion King", "Animation", 10.3f),
            new Book("To Kill a Mockingbird", "Novel", 15f, Collections.singletonList("Harper Lee")),
            new Book("Blackout", "Novel", 31.5f, Arrays.asList("Dhonielle Clayton", "Tiffany D. Jackson",
                    "Nic Stone", "Angie Thomas", "Ashley Woodfolk", "Nicola Yoon"))
    ));
    private List<String> authors = new ArrayList<>();

    public Book(String title) {
        super(title);
    }

    public Book(String title, String category) {
        super(title, category);
    }

    public Book(String title, String category, float cost) {
        super(title, category, cost);
    }


    public Book(String title, String category, float cost, List<String> authors) {
        super(title, category, cost);
        if (checkDupAuthors(authors))
            this.authors = authors;
        else this.authors = null;
    }

    public boolean checkDupAuthors(List<String> authorsList) {
        return (new HashSet<>(authorsList)).size() == authorsList.size();
    }

    public boolean addAuthor(String author) {
        if (this.authors.contains(author)) {
            System.out.println("Duplicate author");
            return false;
        }

        try {
            this.authors.add(author);
            System.out.printf("Added author %s\n\n", author);
            return true;
        } catch (Exception ex) {
            System.out.printf("Cannot add author %s\nUnexpected error", author);
            return false;
        }
    }

    public boolean removeAuthor(String author) {
        if (this.authors.remove(author)) {
            System.out.printf("Removed author %s\n\n", author);
            return true;
        } else {
            System.out.printf("Cannot add author %s\nUnexpected error", author);
            return false;
        }
    }

    @Override
    public String toString() {
        return String.format("Book - %s - %s - %s: $%.2f", getTitle(), getCategory(), authors.toString(), getCost());
    }
}
