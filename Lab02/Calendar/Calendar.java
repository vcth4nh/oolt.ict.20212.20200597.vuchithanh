import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Calendar {
    private static final List<String> monthList = Arrays.asList("January", "February", "March", "April", "May", "June", "July",
            "August", "September", "October", "November", "December", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
            "Aug", "Sep", "Oct", "Nov", "Dec", "Jan.", "Feb.", "Mar.", "Apr.", "May.", "Jun.", "Jul.",
            "Aug.", "Sep.", "Oct.", "Nov.", "Dec.", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12");

    static int countDays(int month, int year) {
        int days = 0;
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                days = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                days = 30;
                break;
            case 2:
                if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
                    days = 29;
                } else {
                    days = 28;
                }
                break;
            default:
                error("month", true);
                break;
        }
        return days;
    }

    public static void error(String attribute, boolean exit) {
        System.out.println("Invalid " + attribute);
        if (exit) System.exit(-1);
    }

    public static void main(String[] args) {
        int year = 0, month;
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Enter month: ");
        String monthStr = keyboard.nextLine();

        month = monthList.indexOf(monthStr) % 12;
        if (month != -1) month++;
        else error("month", true);

        System.out.print("Enter year: ");
        boolean invalidYear = true;
        while (invalidYear)
            try {
                year = keyboard.nextInt();
                invalidYear = false;
            } catch (Exception e) {
                error("year", false);
            }

        System.out.print(monthList.get(month - 1) + ' ' + year + " has " + countDays(month, year) + " days.");
    }
}
