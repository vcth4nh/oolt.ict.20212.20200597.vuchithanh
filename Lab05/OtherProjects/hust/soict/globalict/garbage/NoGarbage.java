package soict.globalict.garbage;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class NoGarbage {
    public static void main(String[] args) {
        long charRead = 0;
        String fileName = "garbage.txt";
        try {
            FileReader myFile = new FileReader(fileName);
            Scanner myReader = new Scanner(myFile);
            StringBuffer sBuffer = new StringBuffer();
            while (myReader.hasNext()) {
                sBuffer.append(myReader.next());
            }
            myReader.close();

            charRead = sBuffer.toString().length();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        System.out.printf("Read %d chars from file %s", charRead, fileName);
    }
}
