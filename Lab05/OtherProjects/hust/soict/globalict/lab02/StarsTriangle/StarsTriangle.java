package soict.globalict.lab02.StarsTriangle;

import java.util.Collections;
import java.util.Scanner;

public class StarsTriangle {

    private static String repeat(String str, int times) {
        return String.join("", Collections.nCopies(times, str));
    }

    private static String padding(int line, int n) {
        return repeat(" ", n - line - 1);
    }

    private static String stars(int line) {
        return repeat("*", line * 2 + 1);
    }

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.print("Enter n: ");
        int n = keyboard.nextInt();

        for (int line = 0; line < n; line++) {
            System.out.printf("%s\n", padding(line, n) + stars(line));
        }

    }
}
