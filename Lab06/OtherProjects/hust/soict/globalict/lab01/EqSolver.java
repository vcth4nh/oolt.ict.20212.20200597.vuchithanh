import javax.swing.*;
import java.awt.GridLayout;
import java.lang.Math;

public class EqSolver {
    public static void main(String[] args) {
        final JRadioButton oneVar = new JRadioButton("First-degree equation with one variable");
        final JRadioButton twoVars = new JRadioButton("System of first-degree equations with two variables");
        final JRadioButton secDegree = new JRadioButton("Second-degree equation with one variable");

        ButtonGroup btnGroup = new ButtonGroup();
        btnGroup.add(oneVar);
        btnGroup.add(twoVars);
        btnGroup.add(secDegree);

        JPanel mode = new JPanel();
        mode.setLayout(new BoxLayout(mode, BoxLayout.Y_AXIS));
        mode.add(oneVar);
        mode.add(twoVars);
        mode.add(secDegree);

        JOptionPane.showMessageDialog(null, mode);

        if (oneVar.isSelected())
            JOptionPane.showMessageDialog(null, firstDegOneVar(), "Result", JOptionPane.INFORMATION_MESSAGE);
        else if (twoVars.isSelected())
            JOptionPane.showMessageDialog(null, firstDegTwoVar(), "Result", JOptionPane.INFORMATION_MESSAGE);
        else if (secDegree.isSelected())
            JOptionPane.showMessageDialog(null, secDegOneVar(), "Result", JOptionPane.INFORMATION_MESSAGE);
        else
            JOptionPane.showMessageDialog(null, "No equation type is selected", "Result", JOptionPane.INFORMATION_MESSAGE);
    }

    public static String firstDegOneVar() {
        JPanel equation = new JPanel();
        equation.setLayout(new GridLayout(1, 4));

        JTextField aStr = new JTextField();
        JTextField bStr = new JTextField();

        equation.add(aStr);
        equation.add(new JLabel("x + "));
        equation.add(bStr);
        equation.add(new JLabel(" = 0"));

        JOptionPane.showMessageDialog(null, equation);

        double a = Double.parseDouble(aStr.getText());
        double b = Double.parseDouble(bStr.getText());

        String result;

        if (a == 0) {
            if (b != 0)
                result = "Many solutions";
            else
                result = "No solutions";
        } else
            result = String.valueOf(-b / a);

        return result;
    }

    public static String firstDegTwoVar() {
        JPanel equation = new JPanel();
        equation.setLayout(new GridLayout(2, 6));

        JTextField a11Str = new JTextField();
        JTextField a12Str = new JTextField();
        JTextField a21Str = new JTextField();
        JTextField a22Str = new JTextField();
        JTextField b1Str = new JTextField();
        JTextField b2Str = new JTextField();

        equation.add(a11Str);
        equation.add(new JLabel("x1 + "));
        equation.add(a12Str);
        equation.add(new JLabel("x2 "));
        equation.add(new JLabel(" = "));
        equation.add(b1Str);
        equation.add(a21Str);
        equation.add(new JLabel("x1 + "));
        equation.add(a22Str);
        equation.add(new JLabel("x2 "));
        equation.add(new JLabel(" = "));
        equation.add(b2Str);

        JOptionPane.showMessageDialog(null, equation);

        double a11 = Double.parseDouble(a11Str.getText());
        double a12 = Double.parseDouble(a12Str.getText());
        double b1 = Double.parseDouble(b1Str.getText());
        double a21 = Double.parseDouble(a21Str.getText());
        double a22 = Double.parseDouble(a22Str.getText());
        double b2 = Double.parseDouble(b2Str.getText());

        double D = a11 * a22 - a21 * a12;
        double D1 = b1 * a22 - b2 * a12;
        double D2 = a11 * b2 - a21 * b1;

        String result;
        if (D == 0) {
            if (D1 == D2)
                result = "Many solutions";
            else
                result = "No solutions";
        } else
            result = ("(x1, x2) = (" + D1 / D + ", " + D2 / D + ")");

        return result;
    }

    public static String secDegOneVar() {
        JPanel equation = new JPanel();
        equation.setLayout(new GridLayout(1, 5));

        JTextField aStr = new JTextField();
        JTextField bStr = new JTextField();
        JTextField cStr = new JTextField();

        equation.add(aStr);
        equation.add(new JLabel("x^2 + "));
        equation.add(bStr);
        equation.add(new JLabel("x + "));
        equation.add(cStr);
        equation.add(new JLabel(" = 0"));

        JOptionPane.showMessageDialog(null, equation);

        double a = Double.parseDouble(aStr.getText());
        double b = Double.parseDouble(bStr.getText());
        double c = Double.parseDouble(cStr.getText());

        String result;
        double delta = b * b - 4 * a * c;
        if (delta > 0)
            result = String.format("(x1, x2) = (%.2f, %.2f)", (-b + Math.sqrt(delta)) / (2 * a), (-b - Math.sqrt(delta)) / (2 * a));
        else if (delta == 0)
            result = String.format("x = %.2f)", (-b / (2 * a)));
        else result = "No solution";

        return result;
    }
}
