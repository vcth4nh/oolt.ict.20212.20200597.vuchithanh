package soict.globalict.aims.order;

import soict.globalict.aims.disc.DigitalVideoDisc;
import soict.globalict.aims.utils.MyDate;

public class Order {
    public static final int MAX_NUMBER_ORDERED = 10;
    private int qtyOrdered = 0;

    public static final int MAX_LIMITED_ORDERS = 5;
    private static int nbOrders = 0;
    private DigitalVideoDisc[] itemsOrdered = new DigitalVideoDisc[MAX_NUMBER_ORDERED];

    private MyDate dateOrdered;

    public Order() {
        if (nbOrders < MAX_LIMITED_ORDERS) {
            nbOrders++;
            dateOrdered = new MyDate();
        } else {
            System.out.printf("Max orders reached (currently %d orders)\n", nbOrders);
            throw new IllegalArgumentException();
        }
    }

    public int getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }

    public MyDate getDateOrdered() {
        return dateOrdered;
    }

    public void setDateOrdered(MyDate dateOrdered) {
        this.dateOrdered = dateOrdered;
    }


    public void addDigitalVideoDisc(DigitalVideoDisc disc) {
        if (qtyOrdered < MAX_NUMBER_ORDERED) {
            itemsOrdered[qtyOrdered] = disc;
            qtyOrdered++;
        } else {
            System.out.println("Max items per order reached");
        }
    }

    public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
        for (int i = 0; i < qtyOrdered; i++) {
            if (itemsOrdered[i] == disc) {
                System.arraycopy(itemsOrdered, i + 1, itemsOrdered, i, qtyOrdered - 1 - i);
            }
        }
        itemsOrdered[qtyOrdered] = null;
        qtyOrdered--;
    }

    public float totalCost() {
        float cost = 0;
        for (int i = 0; i < qtyOrdered; i++) {
            cost += itemsOrdered[i].getCost();
        }
        return cost;
    }

    public void invoiceHead() {
        System.out.println("************************Order************************");
        System.out.printf("Date: %s\n", dateOrdered);
    }

    public void invoiceBody() {
        if (qtyOrdered == 0) {
            System.out.println("Empty order");
        } else
            for (int i = 0; i < qtyOrdered; i++) {
                System.out.printf("%2d. %s\n", i + 1, itemsOrdered[i]);
            }
        System.out.printf("Total cost: %.2f\n", totalCost());
    }

    public void invoiceFoot() {
        System.out.println("*****************************************************");
    }

    public void invoice() {
        invoiceHead();
        invoiceBody();
        invoiceFoot();
    }

    public void invoice(int randDiscNo) {
        invoiceHead();
        if (randDiscNo != -1) {
            System.out.printf("Disc no. %2d (%s) is the lucky item\n", randDiscNo + 1, itemsOrdered[randDiscNo].getTitle());
        }
        invoiceBody();
        invoiceFoot();
    }

    public int getALuckyItem() {
        if (qtyOrdered == 0) return -1;
        int randDiscNo = (int) (Math.random() * qtyOrdered);
        itemsOrdered[randDiscNo].setCost(0);
        return randDiscNo;
    }
}
