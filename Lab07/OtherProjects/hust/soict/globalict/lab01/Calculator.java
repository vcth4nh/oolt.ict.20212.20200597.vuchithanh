import javax.swing.JOptionPane;

public class Calculator {
    public double num1, num2;

    public Calculator() {
        String strNum1, strNum2;
        strNum1 = JOptionPane.showInputDialog(null, "Please input the first number:",
                "Input the first number", JOptionPane.INFORMATION_MESSAGE);
        this.num1 = Double.parseDouble(strNum1);

        strNum2 = JOptionPane.showInputDialog(null, "Please input the second number:",
                "Input the second number", JOptionPane.INFORMATION_MESSAGE);
        this.num2 = Double.parseDouble(strNum2);
    }

    public static void main(String[] args) {
        String strNotification = "Result: \n";
        Calculator cal = new Calculator();

        strNotification += "Sum: " + cal.sum() + '\n';
        strNotification += "Difference: " + cal.diff() + '\n';
        strNotification += "Product: " + cal.product() + '\n';
        strNotification += "Quotient: " + cal.quotient() + '\n';

        JOptionPane.showMessageDialog(null, strNotification,
                "Show Two Numbers", JOptionPane.INFORMATION_MESSAGE);
    }

    public double sum() {
        return this.num1 + this.num2;
    }

    public double diff() {
        return this.num1 - this.num2;
    }

    public double product() {
        return this.num1 * this.num2;
    }

    public double quotient() {
        return this.num1 / this.num2;
    }
}