import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.util.Scanner;

public class MyDate {
    private int day, month, year;

    public MyDate() {
        LocalDateTime now = LocalDateTime.now();
        day = now.getDayOfMonth();
        month = now.getMonthValue();
        year = now.getYear();
    }

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(String dateStr) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d yyyy");
        LocalDate date = LocalDate.parse(dateStr, formatter);
        day = date.getDayOfMonth();
        month = date.getMonthValue();
        year = date.getYear();
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    static public MyDate accept() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input date (e.g. December 13 2003): ");
        return new MyDate(scanner.nextLine());
    }

    public static void main(String[] args) {
        MyDate timeObj1 = new MyDate();
        System.out.printf("No argument: %d-%d-%d\n", timeObj1.day, timeObj1.month, timeObj1.year);

        MyDate timeObj2 = new MyDate(4,4,2002);
        System.out.printf("3 arguments: %d-%d-%d\n", timeObj2.day, timeObj2.month, timeObj2.year);

        MyDate timeObj3 = accept();
        System.out.printf("1 argument: %d-%d-%d\n", timeObj3.day, timeObj3.month, timeObj3.year);

    }
}
