package soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Book extends Media {
    private List<String> authors = new ArrayList<>();

    public Book(String title) {
        super(title);
    }

    public Book(String title, String category) {
        super(title, category);
    }

    public Book(String title, String category, float cost) {
        super(title, category, cost);
    }


    public Book(String title, String category, float cost, List<String> authors) {
        super(title, category, cost);
        if (checkDupAuthors(authors))
            this.authors = authors;
        else this.authors = null;
    }

    public boolean checkDupAuthors(List<String> authorsList) {
        Set<String> authorsSet = new HashSet<>(authorsList);
        return authorsSet.size() == authorsList.size();
    }

    public boolean addAuthor(String author) {
        if (this.authors.contains(author)) {
            return false;
        }

        return this.authors.add(author);
    }

    public boolean removeAuthor(String author) {
        return this.authors.remove(author);
    }

    @Override
    public String toString() {
        return String.format("Book - %s - %s - %s: $%.2f", getTitle(), getCategory(), authors.toString(), getCost());
    }

}
