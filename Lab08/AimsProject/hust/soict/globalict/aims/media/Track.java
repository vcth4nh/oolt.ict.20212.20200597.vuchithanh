package soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Arrays;

public class Track implements Playable, Comparable<Track> {

    static public final ArrayList<Track> availTrack = new ArrayList<>(Arrays.asList(
            new Track("track 1", 10),
            new Track("track 2", 20),
            new Track("track 3", 30)
    ));
    private final String title;
    private int length;

    public String getTitle() {
        return title;
    }

    public int getLength() {
        return length;
    }

    public Track(String title) {
        this.title = title;
    }

    public Track(String title, int length) {
        this.title = title;
        this.length = length;
    }

    @Override
    public void play() {
        System.out.println("Playing track: " + title);
        System.out.println("track length: " + length);
    }

    @Override
    public boolean equals(Object o) {
        if (getClass() != o.getClass())
            return false;

        return title.equals(((Track) o).title) && length == ((Track) o).length;
    }

    @Override
    public int compareTo(Track o) {
        if (getClass() != o.getClass())
            return 0;
        return title.compareTo(o.title);
    }
}
