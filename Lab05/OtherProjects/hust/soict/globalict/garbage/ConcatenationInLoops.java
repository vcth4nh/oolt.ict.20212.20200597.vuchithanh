package soict.globalict.garbage;

public class ConcatenationInLoops {
    int tries1 = 2000;
    int tries2 = tries1 * 1000;
    String[] initStr = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#$%&\\'()*+,-./:;<=>?@[\\\\]^_`{|}~ ".split("");
    String s;

    public ConcatenationInLoops(String s) {
        this.s = s;
    }

    public ConcatenationInLoops() {
        this.s = "";
    }

    public long testPlus(int tries) {
        String s = this.s;
        long start = System.currentTimeMillis();
        for (int i = 0; i < tries; i++) {
            for (String charS : initStr) s += charS;
        }
        return System.currentTimeMillis() - start;
    }

    public long testBuffer(int tries) {
        String s = this.s;
        long start = System.currentTimeMillis();
        StringBuffer sBuffer = new StringBuffer();
        for (int i = 0; i < tries; i++) {
            for (String charS : initStr) sBuffer.append(charS);
        }
        s = sBuffer.toString();
        return System.currentTimeMillis() - start;
    }

    public long testBuilder(int tries) {
        String s = this.s;
        long start = System.currentTimeMillis();
        StringBuilder sBuilder = new StringBuilder();
        for (int i = 0; i < tries; i++) {
            for (String charS : initStr) sBuilder.append(charS);
        }
        s = sBuilder.toString();

        return System.currentTimeMillis() - start;
    }

    public String testBuilder(int tries, int a) {
        long start = System.currentTimeMillis();
        StringBuilder sBuilder = new StringBuilder();
        for (int i = 0; i < tries; i++) {
            for (String charS : initStr) sBuilder.append(charS);
        }
        s = sBuilder.toString();
        return s;
    }

    public static void main(String[] args) {
        ConcatenationInLoops test = new ConcatenationInLoops();
        System.out.printf("Using + operator: %d ms (concat %d times)\n", test.testPlus(test.tries1), test.tries1);
        System.out.printf("Using String Buffer class: %d ms (concat %d times)\n", test.testBuffer(test.tries2), test.tries2);
        System.out.printf("Using String Builder class: %d ms (concat %d times)\n", test.testBuilder(test.tries2), test.tries2);

    }
}
