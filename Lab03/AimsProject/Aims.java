public class Aims {
    public static void main(String[] args) {
        Order anOrder = new Order();

        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Rogers Allers");
        dvd1.setLength(87);
        anOrder.addDigitalVideoDisc(dvd1);

        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star wars", "Science Fiction", "George Lucas");
        dvd2.setCost(24.95f);
        dvd2.setLength(124);
        anOrder.addDigitalVideoDisc(dvd2);

        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation", "John Musker", 90, 18.99f);
        anOrder.addDigitalVideoDisc(dvd3);

        System.out.printf("Total cost is: %.2f\n", anOrder.totalCost());

        System.out.printf("Removing The Lion King with price %.2f\n", dvd1.getCost());
        anOrder.removeDigitalVideoDisc(dvd1);
        System.out.printf("Total cost is: %.2f\n", anOrder.totalCost());
    }
}
