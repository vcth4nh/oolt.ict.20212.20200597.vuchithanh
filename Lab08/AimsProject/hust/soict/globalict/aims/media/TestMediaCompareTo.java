package soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.Collections.sort;
import static soict.globalict.aims.media.Book.availBooks;
import static soict.globalict.aims.media.CompactDisc.availCDs;
import static soict.globalict.aims.media.DigitalVideoDisc.availDiscs;
import static soict.globalict.aims.media.Track.availTrack;

public class TestMediaCompareTo {
    private static void displayCollection(ArrayList<Media> collection) {
        for (Object item : collection) {
            System.out.println(item);
        }
    }


    private static void testSort(ArrayList<Media> list) {
        ArrayList<Media> collection = new ArrayList<>(list);

        System.out.println("Before sorting:");
        displayCollection(collection);

        sort(collection);

        System.out.println("After sorting:");
        displayCollection(collection);
        System.out.println("Done\n\n");
    }


    public static void main(String[] args) {
        testSort(availDiscs);
        testSort(availBooks);
        testSort(availCDs);
    }
}
