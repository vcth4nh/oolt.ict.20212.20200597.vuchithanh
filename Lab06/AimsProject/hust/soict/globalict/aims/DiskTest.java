package soict.globalict.aims;

import soict.globalict.aims.media.DigitalVideoDisc;
import soict.globalict.aims.order.Order;

public class DiskTest {
    public static void main(String[] args) {

        DigitalVideoDisc[] dvd = new DigitalVideoDisc[3];

        dvd[0] = new DigitalVideoDisc("The Lion King");
        dvd[0].setCategory("Animation");
        dvd[0].setCost(19.95f);
        dvd[0].setDirector("Rogers Allers");
        dvd[0].setLength(87);

        dvd[1] = new DigitalVideoDisc("Star wars", "Science Fiction", "George Lucas");
        dvd[1].setCost(24.95f);
        dvd[1].setLength(124);

        dvd[2] = new DigitalVideoDisc("Aladdin", "Animation", "John Musker", 90, 18.99f);

//      -----------------------------

        Order order = new Order();
        order.addMedia(dvd[0]);
        order.addMedia(dvd[1]);
        order.addMedia(dvd[2]);

//      -----------------------------
        String searchTerm = "          wars    star    ";
        System.out.printf("Search for \"%s\" in %s disc: %s\n", searchTerm, dvd[1].getTitle(), dvd[1].search(searchTerm));
        System.out.printf("Search for \"%s\" in %s disc: %s\n", searchTerm, dvd[2].getTitle(), dvd[2].search(searchTerm));
        order.invoice(order.getALuckyItem());
    }
}
