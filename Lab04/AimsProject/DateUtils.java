import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class DateUtils {

    private static int compare(MyDate date1, MyDate date2) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d M yyyy");
        String date1Str = String.format("%d %d %d", date1.getDay(), date1.getMonth(), date1.getYear());
        LocalDate realDate1 = LocalDate.parse(date1Str, formatter);
        String date2Str = String.format("%d %d %d", date2.getDay(), date2.getMonth(), date2.getYear());
        LocalDate realDate2 = LocalDate.parse(date2Str, formatter);
        return realDate1.compareTo(realDate2);
    }

    public static void printCompare(MyDate date1, MyDate date2) {
        int cmpRes = compare(date1, date2);
        if (cmpRes > 0) {
            System.out.printf("%s occurs after %s", date1, date2);
        } else if (cmpRes < 0) {
            System.out.printf("%s occurs before %s", date1, date2);
        } else {
            System.out.println("Both dates are equal");
        }
    }

    public static void sort(List<MyDate> dateList) {
        dateList.sort(DateUtils::compare);
    }
}
