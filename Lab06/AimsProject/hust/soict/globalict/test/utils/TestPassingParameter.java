package soict.globalict.test.utils;

import soict.globalict.aims.media.DigitalVideoDisc;

public class TestPassingParameter {
    public static void main(String[] args) {
        DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
        DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");
        swap(jungleDVD, cinderellaDVD);
        System.out.println("Swap func:");
        System.out.println("jungle dvd title: " + jungleDVD.getTitle());
        System.out.println("cinderella dvd title: " + cinderellaDVD.getTitle());

        System.out.println("Real swap func:");
        DiscObjWrapper discWrap1 = new DiscObjWrapper(jungleDVD);
        DiscObjWrapper discWrap2 = new DiscObjWrapper(cinderellaDVD);
        realSwap(discWrap1, discWrap2);
        System.out.println("jungle dvd title: " + discWrap1.disc.getTitle());
        System.out.println("cinderella dvd title: " + discWrap2.disc.getTitle());

        System.out.println("Change title: ");
        changeTitle(jungleDVD, cinderellaDVD.getTitle());
        System.out.println("jungle dvd title: " + jungleDVD.getTitle());

    }

    public static void swap(Object o1, Object o2) {
        Object tmp = o1;
        o1 = o2;
        o2 = tmp;
    }

    public static void realSwap(DiscObjWrapper d1, DiscObjWrapper d2) {
        DigitalVideoDisc tmp = d1.disc;
        d1.disc = d2.disc;
        d2.disc = tmp;
    }

    public static void changeTitle(DigitalVideoDisc dvd, String title) {
        String oldTitle = dvd.getTitle();
        dvd.setTitle(title);
        dvd = new DigitalVideoDisc(oldTitle);
    }

    private static class DiscObjWrapper {
        public DigitalVideoDisc disc;

        DiscObjWrapper(DigitalVideoDisc disc) {
            this.disc = disc;
        }
    }
}
