import java.util.Arrays;
import java.util.List;

public class DateTest {
    public static void main(String[] args) {
        MyDate date;
        System.out.println("First date:");
        try {
            date = new MyDate("nineteenth", "April", "twenty ten");
        } catch (Exception e) {
            System.out.println("Not a valid date format");
            return;
        }
        date.print();
        System.out.println("\nTest date formatter:");
        date.print("yyyy-MM-dd");
        date.print("d/M/yyyy");
        date.print("MMM d yyyy");
        date.print("MM-dd-yyyy");

        MyDate date2;
        System.out.println("\n\nSecond date:");
        try {
            date2 = new MyDate(2, 3, 2021);
        } catch (Exception e) {
            System.out.println("Not a valid date format");
            return;
        }
        date2.print();
        System.out.println("\nTest compare 2 date:");
        DateUtils.printCompare(date, date2);

        System.out.println("\n\nTest date sort:\nBefore sorting:");
        List<MyDate> dateList = Arrays.asList(new MyDate(31, 8, 2999), date, date2, new MyDate(14, 7, 2020));
        System.out.println("dateList = " + dateList);
        System.out.println("After sorting:");
        DateUtils.sort(dateList);
        System.out.println("dateList = " + dateList);
    }
}
