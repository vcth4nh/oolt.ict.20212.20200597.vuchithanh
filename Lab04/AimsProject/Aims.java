import sun.java2d.windows.GDIWindowSurfaceData;

import java.util.Random;

public class Aims {
    public static void main(String[] args) {

        DigitalVideoDisc[] dvd = new DigitalVideoDisc[3];

        dvd[0] = new DigitalVideoDisc("The Lion King");
        dvd[0].setCategory("Animation");
        dvd[0].setCost(19.95f);
        dvd[0].setDirector("Rogers Allers");
        dvd[0].setLength(87);

        dvd[1] = new DigitalVideoDisc("Star wars", "Science Fiction", "George Lucas");
        dvd[1].setCost(24.95f);
        dvd[1].setLength(124);

        dvd[0] = new DigitalVideoDisc("Aladdin", "Animation", "John Musker", 90, 18.99f);

//      ----------------------------------------

        Order[] ordersList = new Order[10];

        for (int i = 0; i <= 10; i++) {
            Random random = new Random();
            try {
                System.out.printf("\nCreate order no.%d\n", i + 1);
                ordersList[i] = new Order();
                int numberOfDiscs = random.nextInt(15 + 10) - 10;
                for (int discNo = 1; discNo <= numberOfDiscs; discNo++) {
                    int discTypeNo = random.nextInt(2);
                    ordersList[i].addDigitalVideoDisc(dvd[discTypeNo]);
                }
            } catch (Exception ignore) {
            }
        }

        System.out.println("\n\nInvoice:");

        for (Order order : ordersList) {
            if (order == null) return;

            order.invoice();
            System.out.println();
        }
    }
}
