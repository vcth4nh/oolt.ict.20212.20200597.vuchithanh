package soict.globalict.aims.utils;

public class MemoryDaemon implements Runnable {
    long memoryUsed;

    @Override
    public void run() {
        Runtime rt = Runtime.getRuntime();
        long used;

        while (true) {
            used = (rt.totalMemory() - rt.freeMemory()) / 1024; // Kilobytes

            if (used != memoryUsed) {
                System.out.println("\t\t\t\t\tMemory used = " + used);
                memoryUsed = used;
            }
        }
    }
}
