package soict.globalict.lab02.MatrixSummation;

public class MatrixSummation {
    private static void printMatrix(int[][] matrix) {
        for (int[] row : matrix) {
            for (int cell : row) {
                System.out.printf("%3d ", cell);
            }
            System.out.println();
        }
    }

    private static void error() {
        System.out.println("Matrices sizes are not equal");
        System.exit(-1);
    }

    private static int[][] sum(int[][] matrix1, int[][] matrix2) {
        int rowCount, colCount = 0;
        if ((rowCount = matrix1.length) != matrix2.length || (colCount = matrix1[0].length) != matrix2[0].length)
            error();

        int[][] sum = new int[rowCount][colCount];
        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < colCount; j++) {
                sum[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }
        return sum;
    }


    public static void main(String[] args) {
        int[][] matrix1 = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] matrix2 = {{4, 5, 6}, {7, 8, 9}, {10, 11, 12}};

        System.out.println("Array 1:");
        printMatrix(matrix1);
        System.out.println("Array 2:");
        printMatrix(matrix2);

        System.out.println("Sum of 2 array:");
        printMatrix(sum(matrix1, matrix2));

    }
}
